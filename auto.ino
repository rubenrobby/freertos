#include <Servo.h>

#include <Arduino_FreeRTOS.h>

// define two tasks for Blink & AnalogRead
void TaskBlink( void *pvParameters );
void TaskAnalogRead( void *pvParameters );
void TaskRead( void *pvParameters);
void TaskSweep( void *pvParameters);
char pinkLeft;
char pinkRight;
char sweep;
Servo myServo;
// the setup function runs once when you press reset or power the board
void setup() {
  myServo.attach(3);
  
  pinkLeft = 0;
  pinkRight = 0;
  // Now set up two tasks to run independently.
  xTaskCreate(
    TaskBlink
    ,  (const portCHAR *)"Blink"   // A name just for humans
    ,  128  // Stack size
    ,  NULL
    ,  2  // priority
    ,  NULL );

  xTaskCreate(
    TaskRead
    ,  (const portCHAR *) "Analog"
    ,  128 // This stack size can be checked & adjusted by reading Highwater
    ,  NULL
    ,  2  // priority
    ,  NULL );

   xTaskCreate(
    TaskSweep
    ,  (const portCHAR *) "AnalogRead"
    ,  128 // This stack size can be checked & adjusted by reading Highwater
    ,  NULL
    ,  2  // priority
    ,  NULL );

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/


void TaskSweep(void *pvParameters){
  (void) pvParameters;
  int pos = 10;
  for(;;){
    if(sweep){
      for (pos = 10; pos <= 180; pos += 10) { // goes from 0 degrees to 180 degrees
        // in steps of 1 degree
        myServo.write(pos);              // tell servo to go to position in variable 'pos'
        vTaskDelay(2);                       // waits 15ms for the servo to reach the position
      }
      for (pos = 180; pos >= 10; pos -= 10) { // goes from 180 degrees to 0 degrees
        myServo.write(pos);                   // tell servo to go to position in variable 'pos'
        vTaskDelay(2);                        // waits 15ms for the servo to reach the position
      }
    } else {
      while(pos >= 10) { // goes from 180 degrees to 0 degrees
        myServo.write(pos);
        pos -= 5;// tell servo to go to position in variable 'pos'
        vTaskDelay(1);                    // waits 15ms for the servo to reach the position
      }
    }
  }
}

void TaskRead(void *pvParameters){
  (void) pvParameters;
  pinMode(12, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  Serial.begin(9600);
  int analog = 0;

  for(;;){
    pinkLeft = digitalRead(12);
    pinkRight = digitalRead(11);
    sweep = digitalRead(8);
    analog = analogRead(0);
    Serial.println(analog);
    vTaskDelay(1);
  }
}

void TaskBlink(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  // initialize digital pin 13 as an output.
  
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);

  for (;;) // A Task shall never return or exit.
  {
    if(pinkRight){
      digitalWrite(10, HIGH);
    };
    if(pinkLeft){
      digitalWrite(9, HIGH);
    };
    vTaskDelay( 500 / portTICK_PERIOD_MS ); // wait for one second
    digitalWrite(10, LOW);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(9, LOW);    // turn the LED off by making the voltage LOW
    vTaskDelay( 500 / portTICK_PERIOD_MS ); // wait for one second
  }
}

void TaskAnalogRead(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  for (;;)
  {
    // read the input on analog pin 0:
    int potentioValue = analogRead(A0);
    int gasPedaal = analogRead(A1);
    int remPedaal = analogRead(A2);
    // print out the value you read:
    Serial.println(sensorValue);
    vTaskDelay(pdMS_TO_TICKS(20));  // one tick delay (15ms) in between reads for stability
  }
}

void TaskPinkers(void *pvParameters){
  (void) pvParameters;
  pinMode(12, INPUT_PULLUP);
  pinMode(11, INPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  

    for(;;){
      int val1 = digitalRead(12);
      int val2 = digitalRead(11);
    }
}

void TaskGasRem(void *pvParameters){
  pinMode  
}


